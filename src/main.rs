use std::collections::HashSet;
use std::io::StdinLock;
use std::{
    cmp,
    collections::HashMap,
    io::{self, BufRead, Lines},
};

fn main() {
    day_25::day_25();
}

mod day_25 {
    use std::io::{self, BufRead};

    pub fn day_25() {
        let lines: Vec<Vec<char>> = io::stdin()
            .lock()
            .lines()
            .map(Result::unwrap)
            .map(|s| s.chars().filter(|c| *c != '\n').collect())
            .collect();
        day_25_pt1(lines);
    }

    fn day_25_pt1(mut field: Vec<Vec<char>>) {
        let mut cnt = 0;
        loop {
            let new_field = step(&field);
            cnt += 1;
            /*
            new_field
                .iter()
                .for_each(|row| println!("{}", row.iter().collect::<String>()));
            println!("After step {}", cnt);
            */
            if is_unchanged(&new_field, &field) {
                break;
            }
            field = new_field;
        }

        println!("First step on which no sea cucumbers move is {}", cnt);
    }

    fn step(field: &Vec<Vec<char>>) -> Vec<Vec<char>> {
        let width = field[0].len();
        let length = field.len();
        let eastbound_herd_moved: Vec<Vec<char>> = field
            .iter()
            .map(|row| {
                let mut row_moved = vec!['.'; width];
                for i in 0..width {
                    let next_pos = (i + 1) % width;
                    if row[i] == '>' && row[next_pos] == '.' {
                        row_moved[i] = '.';
                        row_moved[next_pos] = '>';
                    } else if row_moved[i] == '.' {
                        row_moved[i] = row[i];
                    }
                }
                row_moved
            })
            .collect();
        let mut southbound_herd_moved: Vec<Vec<char>> = vec![vec!['.'; width]; length];

        for c in 0..width {
            for l in 0..length {
                let next_pos = (l + 1) % length;
                if eastbound_herd_moved[l][c] == 'v' && eastbound_herd_moved[next_pos][c] == '.' {
                    southbound_herd_moved[l][c] = '.';
                    southbound_herd_moved[next_pos][c] = 'v';
                } else if southbound_herd_moved[l][c] == '.' {
                    southbound_herd_moved[l][c] = eastbound_herd_moved[l][c];
                }
            }
        }
        southbound_herd_moved
    }

    fn is_unchanged(new_field: &Vec<Vec<char>>, field: &Vec<Vec<char>>) -> bool {
        *new_field == *field
    }
}

fn day_07() {
    let std_in = io::stdin();
    let mut handle = std_in.lock().lines();
    let number_line = handle.next().unwrap();
    let number_line = number_line.unwrap();
    let positions: Vec<isize> = number_line
        .split(',')
        .map(str::parse::<isize>)
        .map(Result::unwrap)
        .collect();
    day_07_pt1(positions.clone());
    day_07_pt2(positions.clone());
}

fn day_07_pt2(positions: Vec<isize>) {
    let max_pos = positions.iter().max().unwrap();
    let differences_per_position = (0..max_pos + 1)
        .fold(
            (HashMap::new(), HashMap::new()),
            |(mut acc, diff_cache), pos| {
                let positions_for_diff = positions.clone();
                let (diff_sum, diff_cache) =
                    positions_for_diff
                        .iter()
                        .fold((0, diff_cache), |(acc, diff_cache), &i| {
                            let diff = (pos - i).abs();
                            let (val, next_diff_cache) = match diff_cache.get(&diff) {
                                Some(&n) => (n, diff_cache.clone()),
                                None => calculate_fuel(diff, diff_cache.clone()),
                            };
                            (acc + val, next_diff_cache)
                        });
                acc.insert(pos, diff_sum);
                (acc, diff_cache)
            },
        )
        .0;

    let min_diff_sum =
        differences_per_position
            .into_iter()
            .fold(isize::max_value(), |acc, (_, diff_sum)| {
                if diff_sum < acc {
                    diff_sum
                } else {
                    acc
                }
            });
    println!("Minimal difference pt2 is {}", min_diff_sum);
}

fn calculate_fuel(diff: isize, cache: HashMap<isize, isize>) -> (isize, HashMap<isize, isize>) {
    (0..diff).fold((0, cache), |(acc, mut this_cache), i| {
        let this_diff = i + 1;
        let val = acc + this_diff;

        this_cache.insert(this_diff, val);
        (val, this_cache)
    })
}

fn day_07_pt1(positions: Vec<isize>) {
    println!("Number of positions is {}", positions.len());
    let differences_per_position = positions.iter().fold(HashMap::new(), |mut acc, &pos| {
        let positions_for_diff = positions.clone();
        let diff_sum = positions_for_diff
            .iter()
            .fold(0, |acc, &i| acc + if pos >= i { pos - i } else { i - pos });
        /*
        let val = match acc.get(&pos) {
            Some(&n) => n + diff_sum,
            None => diff_sum,
        };
        */
        acc.insert(pos, diff_sum);
        acc
    });

    let min_diff_sum =
        differences_per_position
            .into_iter()
            .fold(isize::max_value(), |acc, (_, diff_sum)| {
                if diff_sum < acc {
                    diff_sum
                } else {
                    acc
                }
            });
    println!("Minimal difference is {}", min_diff_sum);
}

fn day_06() {
    let std_in = io::stdin();
    let mut handle = std_in.lock().lines();
    let number_line = handle.next().unwrap();
    let number_line = number_line.unwrap();
    let seed_values: Vec<u8> = number_line
        .split(',')
        .map(str::parse::<u8>)
        .map(Result::unwrap)
        .collect();
    day_06_pt1(seed_values.clone());
    day_06_pt2(seed_values);
}

fn day_06_pt2(seed_values: Vec<u8>) {
    let unique_seed_values: HashSet<u8> =
        seed_values
            .iter()
            .fold(HashSet::new(), |mut acc, &seed_value| {
                acc.insert(seed_value);
                acc
            });

    let days = 256;
    let seed_values_counts: HashMap<u8, usize> =
        unique_seed_values
            .iter()
            .fold(HashMap::new(), |mut acc, &seed_value| {
                println!("Seed value is {}", seed_value);
                let count = calculate_day(days, seed_value);
                println!("Count of seed value is {}", count);
                acc.insert(seed_value, count);
                acc
            });
    let overall_count = seed_values.iter().fold(0, |acc, &seed_value| {
        let count = *seed_values_counts.get(&seed_value).unwrap();
        acc + count
    });

    println!(
        "Number of lanternfish after {} days is {}",
        days, overall_count
    );
}

fn day_06_pt2c(seed_values: Vec<u8>) {
    let days = 256;
    let count = seed_values.iter().fold(0, |acc, &seed_value| {
        println!("Seed value is {}", seed_value);
        acc + step(days, seed_value.into())
    });

    println!("Number of lanternfish after {} days is {}", days, count);
}
fn step(days: isize, seed_value: isize) -> isize {
    let remaining_days = if days - seed_value >= 0 {
        days - seed_value
    } else {
        0
    };
    let direct_children = calculate_direct_children(remaining_days);
    let mut count = 0;
    for each in 0..direct_children {
        count += step(remaining_days - (each * 7), 8)
    }
    count
}

fn calculate_direct_children(remaining_days: isize) -> isize {
    (remaining_days / 7 + if remaining_days % 7 > 0 { 1 } else { 0 }).into()
}
fn day_06_pt2b(seed_values: Vec<u8>) {
    let days = 256;
    let count = seed_values.iter().fold(0, |acc, &seed_value| {
        println!("Seed value is {}", seed_value);
        acc + calculate_day(days, seed_value)
    });

    println!("Number of lanternfish after {} days is {}", days, count);
}

fn calculate_day(days_left: u16, timer_value: u8) -> usize {
    if days_left == 0 {
        1
    } else {
        if timer_value == 0 {
            calculate_day(days_left - 1, 6) + calculate_day(days_left - 1, 8)
        } else {
            calculate_day(days_left - 1, timer_value - 1)
        }
    }
}

fn day_06_pt1(individuals: Vec<u8>) {
    println!("Number of seed values is {}", individuals.len());

    let count = (0..80)
        .fold(individuals, |acc, _day| {
            acc.iter()
                .map(|&i| if i == 0 { vec![6, 8] } else { vec![i - 1] })
                .flatten()
                .collect::<Vec<u8>>()
        })
        .len();

    println!("Number of lanternfish after 80 days is {}", count);
}

fn day_05() {
    let lines: Vec<((usize, usize), (usize, usize))> = io::stdin()
        .lock()
        .lines()
        .map(Result::unwrap)
        .map(|s| {
            let s = s.to_string();
            let mut parts = s.split(" -> ");
            let start = parts.next().unwrap();
            let end = parts.next().unwrap();
            (start.to_string(), end.to_string())
        })
        .map(|(start, end)| (to_coord_pair(start), to_coord_pair(end)))
        .collect();
    day_05_pt1(&lines);
    day_05_pt2(&lines);
}

fn day_05_pt2(lines: &Vec<((usize, usize), (usize, usize))>) {
    let overlap_count = lines_to_overlap_count(lines, to_points_horiz_vert_diag);
    println!(
        "Number of points where at least two lines (horiz., vert., diag.) overlap is {}",
        overlap_count
    );
}

fn to_points_horiz_vert_diag(
    ((x1, y1), (x2, y2)): &((usize, usize), (usize, usize)),
) -> Vec<(usize, usize)> {
    let x1 = *x1 as isize;
    let y1 = *y1 as isize;
    let x2 = *x2 as isize;
    let y2 = *y2 as isize;
    let mut result = vec![];
    if x1 == x2 {
        let start_y = cmp::min(y1, y2);
        let end_y = cmp::max(y1, y2);
        (start_y..(end_y + 1)).for_each(|y| result.push((x1 as usize, y as usize)));
    }
    if y1 == y2 {
        let start_x = cmp::min(x1, x2);
        let end_x = cmp::max(x1, x2);
        (start_x..(end_x + 1)).for_each(|x| result.push((x as usize, y1 as usize)));
    }
    let diff_x = (x1 - x2).abs();
    let diff_y = (y1 - y2).abs();

    if diff_x == diff_y {
        let ((start_x, start_y), (end_x, end_y)) = if x1 < x2 {
            ((x1, y1), (x2, y2))
        } else {
            ((x2, y2), (x1, y1))
        };
        let y_range = if start_y > end_y {
            end_y..(start_y + 1)
        } else {
            start_y..(end_y + 1)
        };
        let y_range = y_range.map(|v| {
            if start_y > end_y {
                start_y - (v - end_y)
            } else {
                v
            }
        });

        (start_x..(end_x + 1)).zip(y_range).for_each(|(x, y)| {
            result.push((x as usize, y as usize));
        });
    }
    result
}

fn day_05_pt1(lines: &Vec<((usize, usize), (usize, usize))>) {
    let overlap_count = lines_to_overlap_count(lines, to_points_horiz_vert);
    println!(
        "Number of points where at least two lines overlap is {}",
        overlap_count
    );
}

fn lines_to_overlap_count(
    lines: &Vec<((usize, usize), (usize, usize))>,
    to_points: fn(&((usize, usize), (usize, usize))) -> Vec<(usize, usize)>,
) -> usize {
    let field = lines
        .iter()
        .map(to_points)
        // .inspect(|points| points.iter().for_each(|(x, y)| println!("({},{})", x, y)))
        .fold(HashMap::new(), |mut acc, points| {
            points.into_iter().for_each(|p| {
                let count = match acc.get(&p) {
                    Some(v) => v + 1,
                    None => 1,
                };
                acc.insert(p, count);
            });
            acc
        });
    let overlap_count = field.into_iter().filter(|(_, v)| *v > 1).count();
    overlap_count
}

fn to_points_horiz_vert(
    ((x1, y1), (x2, y2)): &((usize, usize), (usize, usize)),
) -> Vec<(usize, usize)> {
    let x1 = *x1;
    let y1 = *y1;
    let x2 = *x2;
    let y2 = *y2;
    let mut result = vec![];
    if x1 == x2 {
        let start_y = cmp::min(y1, y2);
        let end_y = cmp::max(y1, y2);
        (start_y..(end_y + 1)).for_each(|y| result.push((x1, y)));
    }
    if y1 == y2 {
        let start_x = cmp::min(x1, x2);
        let end_x = cmp::max(x1, x2);
        (start_x..(end_x + 1)).for_each(|x| result.push((x, y1)));
    }
    result
}

fn to_coord_pair(point_str: String) -> (usize, usize) {
    let mut parts = point_str.split(',');
    let x = parts.next().unwrap().parse::<usize>().unwrap();
    let y = parts.next().unwrap().parse::<usize>().unwrap();

    (x, y)
}

fn day_04() {
    let std_in = io::stdin();
    let mut handle = std_in.lock().lines();
    let number_line = handle.next().unwrap();
    let number_line = number_line.unwrap();
    let random_numbers = number_line
        .split(',')
        .map(str::parse::<u8>)
        .map(Result::unwrap);
    handle.next();
    let mut boards = vec![];
    while let Some(board) = read_board(&mut handle) {
        boards.push(board);
    }
    /*
        boards.get(0).unwrap().iter().for_each(|r| {
            r.iter().for_each(|(v, f)| print!("({},{})", v, f));
            println!("");
        });
    */
    day04_pt1(boards.clone(), random_numbers.clone());
    day04_pt2(boards.clone(), random_numbers.clone());
}

fn day04_pt2<I>(mut boards: Vec<Vec<Vec<(u8, bool)>>>, mut random_numbers: I)
where
    I: Iterator<Item = u8>,
    I::Item: Into<u8>,
{
    random_numbers
        .find(|n| {
            boards = mark_numbers(boards.clone(), *n);
            if let Some(Some(score)) = boards
                .iter()
                .map(|b| check_board(b, *n))
                .find(Option::is_some)
            {
                if boards.len() == 1 {
                    println!("Score of last winning board is {}", score);
                    true
                } else {
                    boards = boards
                        .clone()
                        .into_iter()
                        .filter(|b| check_board(b, *n).is_none())
                        .collect();
                    false
                }
            } else {
                false
            }
        })
        .unwrap();
}

fn day04_pt1<I>(mut boards: Vec<Vec<Vec<(u8, bool)>>>, mut random_numbers: I)
where
    I: Iterator<Item = u8>,
    I::Item: Into<u8>,
{
    random_numbers
        .find(|n| {
            boards = mark_numbers(boards.clone(), *n);
            if let Some(Some(score)) = boards
                .iter()
                .map(|b| check_board(b, *n))
                .find(Option::is_some)
            {
                println!("Score is {}", score);
                true
            } else {
                false
            }
        })
        .unwrap();
}

fn check_board(board: &Vec<Vec<(u8, bool)>>, last_number: u8) -> Option<usize> {
    let has_completed_row = board.iter().map(|l| l.iter().all(|(_, m)| *m)).any(|r| r);
    let has_completed_column = (0..5)
        .map(|c| {
            board
                .iter()
                .map(|l| l.iter().nth(c).unwrap())
                .all(|(_, m)| *m)
        })
        .any(|r| r);
    if has_completed_column || has_completed_row {
        let sum_unmarked_numbers: usize = board
            .iter()
            .map(|l| {
                l.iter()
                    .map(|(v, m)| if !m { usize::from(*v) } else { 0 })
                    .sum::<usize>()
            })
            .sum();
        Some(sum_unmarked_numbers * usize::from(last_number))
    } else {
        None
    }
}

fn mark_numbers(boards: Vec<Vec<Vec<(u8, bool)>>>, number: u8) -> Vec<Vec<Vec<(u8, bool)>>> {
    boards
        .into_iter()
        .map(|b| {
            b.into_iter()
                .map(|l| {
                    l.into_iter()
                        .map(|(v, m)| if v == number { (v, true) } else { (v, m) })
                        .collect()
                })
                .collect()
        })
        .collect()
}

fn read_board(handle: &mut Lines<StdinLock>) -> Option<Vec<Vec<(u8, bool)>>> {
    let mut board = vec![];
    for line in handle {
        let line = line.unwrap();
        if line.is_empty() {
            break;
        }
        board.push(
            line.split_whitespace()
                .map(str::parse::<u8>)
                .map(Result::unwrap)
                .map(|n| (n, false))
                .collect(),
        );
    }
    if board.is_empty() {
        None
    } else {
        Some(board)
    }
}

fn day_03() {
    let lines: Vec<Vec<char>> = io::stdin()
        .lock()
        .lines()
        .map(Result::unwrap)
        .map(|s| s.chars().collect::<Vec<char>>())
        .collect();
    day_03_pt1(&lines);
    day_03_pt2(&lines);
}

fn day_03_pt2(lines: &Vec<Vec<char>>) {
    let width = lines.get(0).unwrap().len();
    let lines: Vec<Vec<u8>> = lines
        .iter()
        .map(|v| {
            v.iter()
                .map(|c| match c {
                    '1' => 1,
                    _ => 0,
                })
                .collect()
        })
        .collect();
    let oxygen_generator_rating = find_values_at_pos(&lines, 0, true)
        .into_iter()
        .nth(0)
        .unwrap()
        .into_iter()
        .map(usize::from)
        .enumerate()
        .fold(0, |acc, (pos, bit)| acc + (bit << (width - (pos + 1))));

    let co2_scubber_rating = find_values_at_pos(&lines, 0, false)
        .into_iter()
        .nth(0)
        .unwrap()
        .into_iter()
        .map(usize::from)
        .enumerate()
        .fold(0, |acc, (pos, bit)| acc + (bit << (width - (pos + 1))));

    println!(
        "Life support rating is {}",
        oxygen_generator_rating * co2_scubber_rating
    );
}

fn find_values_at_pos(lines: &Vec<Vec<u8>>, pos: usize, pick_most_common: bool) -> Vec<Vec<u8>> {
    let count = lines.iter().fold((0, 0), |(one_count, zero_count), bits| {
        if *bits.get(pos).unwrap() == 1 {
            (one_count + 1, zero_count)
        } else {
            (one_count, zero_count + 1)
        }
    });

    let (one_count, zero_count) = count;

    let lines: Vec<Vec<u8>> = lines
        .iter()
        .filter(|bits| {
            let dgt = *bits.get(pos).unwrap();
            if pick_most_common {
                (one_count >= zero_count && dgt == 1) || (one_count < zero_count && dgt == 0)
            } else {
                (one_count < zero_count && dgt == 1) || (one_count >= zero_count && dgt == 0)
            }
        })
        .map(Vec::clone)
        .collect();

    if lines.len() == 1 {
        lines
    } else {
        find_values_at_pos(&lines, pos + 1, pick_most_common)
    }
}

fn day_03_pt1(lines: &Vec<Vec<char>>) {
    let width = lines.get(0).unwrap().len();
    let counts = lines.iter().fold(vec![0; width], |acc, bits| {
        bits.iter()
            .zip(acc.iter())
            .map(|(chr, count)| if *chr == '1' { *count + 1 } else { *count })
            .collect()
    });

    let threshold = lines.len() / 2;
    let gamma = counts
        .iter()
        .map(|count| if *count >= threshold { 1 } else { 0 })
        .enumerate()
        .fold(0, |acc, (pos, bit)| acc + (bit << (width - (pos + 1))));

    let mask = vec![1; width]
        .iter()
        .enumerate()
        .fold(0, |acc, (pos, bit)| acc + (bit << (width - (pos + 1))));
    let epsilon = !gamma & mask;

    println!("Gamma is {:#014b}, epsilon is {:#014b}", gamma, epsilon);
    println!("Power consumption is {}", gamma * epsilon);
}

fn day_02() {
    let lines: Vec<(String, usize)> = io::stdin()
        .lock()
        .lines()
        .map(Result::unwrap)
        .map(|s| {
            let mut splitter = s.split(" ");
            (
                splitter.next().unwrap().to_string(),
                splitter.next().unwrap().parse::<usize>().unwrap(),
            )
        })
        .collect();
    day_02_pt1(&lines);
    day_02_pt2(&lines);
}

fn day_02_pt2(lines: &Vec<(String, usize)>) {
    let (result_horiz, result_depth, _result_aim) =
        lines
            .iter()
            .fold((0, 0, 0), |(horiz, depth, aim), (cmd, amount)| {
                match cmd.as_str() {
                    "forward" => (horiz + amount, depth + aim * amount, aim),
                    "up" => (horiz, depth, aim - amount),
                    "down" => (horiz, depth, aim + amount),
                    _ => (horiz, depth, aim),
                }
            });

    println!(
        "Final product (using aim) is {}",
        result_horiz * result_depth
    );
}

fn day_02_pt1(lines: &Vec<(String, usize)>) {
    let (result_horiz, result_depth) =
        lines
            .iter()
            .fold((0, 0), |(horiz, depth), (cmd, amount)| match cmd.as_str() {
                "forward" => (horiz + amount, depth),
                "up" => (horiz, depth - amount),
                "down" => (horiz, depth + amount),
                _ => (horiz, depth),
            });

    println!("Final product is {}", result_horiz * result_depth);
}

fn day_01() {
    let lines: Vec<usize> = io::stdin()
        .lock()
        .lines()
        .map(Result::unwrap)
        .map(|s| s.parse::<usize>())
        .map(Result::unwrap)
        .collect();
    let increased_count = count_increased(&lines);
    let sliding_window_increased_count = count_sliding_window_increased(&lines);
    println!("Increased count is {}", increased_count);
    println!(
        "Sliding window increased count is {}",
        sliding_window_increased_count
    );
}

fn count_increased(lines: &Vec<usize>) -> usize {
    lines
        .iter()
        .zip(lines[1..].iter())
        .map(|(a, b)| *b > *a)
        .filter(|result| *result)
        .count()
}

fn count_sliding_window_increased(lines: &Vec<usize>) -> usize {
    let three_measurements_sliding_window: Vec<usize> = lines
        .iter()
        .zip(lines[1..].iter().zip(lines[2..].iter()))
        .map(|(a, (b, c))| *a + *b + *c)
        .collect();
    count_increased(&three_measurements_sliding_window)
}
